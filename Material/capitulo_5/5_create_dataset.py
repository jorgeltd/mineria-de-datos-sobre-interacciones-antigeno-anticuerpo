'''
script que permite la lectura del archivo y crea un documento en formato fasta con la informacion de los antigenos
'''

import pandas as pd
import sys

#funcion que permite contar la frecuencia de un residuo
def countFreqResidue(seq, residue):

    count=0
    for res in seq:
        if res == residue:
            count+=1
    return count

data = pd.read_csv(sys.argv[1])

fileData = open('antigen_filter.fasta', 'w')
fileData2 = open('antigen_filter_seq.txt', 'w')

for i in range(len(data)):
    antigen = str(data['antigen'][i])
    seq = str(data['seq'][i])
    seq = seq.replace(".", "")
    seq = seq.replace("?", "")

    fileData.write(">"+antigen+"\n")
    fileData.write(seq+"\n")
    fileData2.write(seq+"\n")
fileData.close()
fileData2.close()

#hacemos un set de datos con la frecuencia de los residuos y el largo de los elementos
matrixResponse = []
matrixAntigenID = []

listAA = "ARNDCQEGHILKMFPSTWYV"

print(len(listAA))

for i in range(len(data)):
    try:
        row = []
        for element in listAA:
            row.append(countFreqResidue(data['seq'][i], element))

        row.append(len(data['seq'][i]))
        matrixResponse.append(row)
        matrixAntigenID.append([data['antigen'][i]])
    except:
        pass
columnsHeader = list(listAA)
columnsHeader.append("length")

dataFreq = pd.DataFrame(matrixResponse, columns=columnsHeader)
dataFreqIndex = pd.DataFrame(matrixAntigenID, columns=['antigenID'])

dataFreq.to_csv("dataSetFrequence.csv", index=False)
dataFreqIndex.to_csv("dataSetFrequenceIndex.csv", index=False)
