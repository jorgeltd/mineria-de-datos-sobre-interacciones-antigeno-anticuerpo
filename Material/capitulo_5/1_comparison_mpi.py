from mpi4py import MPI
import pandas as pd
import numpy as np
import os
from scipy import stats
from scipy.special import comb
import itertools
import math
import time
import sys
import csv


def _get_data(df, ty):
    return df[(df['Type'] == ty) & (df['Bad_Data'] == False)]


def _u_test(s1, s2, sig):
    u, p = stats.mannwhitneyu(s1, s2, alternative='two-sided')
    sts = 0
    if p < sig:
        sts = 1

    return u, p, sts


def divide_chunks(l, n):
    # looping till length l
    for i in range(0, len(l), n):
        yield l[i:i + n]


def split(anti_list, n_parts):
    l_ = [(x, y) for x, y in itertools.combinations(anti_list, 2)]
    return [l_[i::n_parts] for i in range(n_parts)]


# srun -N 2 -n 40 python script.py results_log dataset/all.csv 34
def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    result_dir = sys.argv[1]
    data_path = sys.argv[2]
    antigen_rate = int(sys.argv[3])

    print('Proc {} leyendo data'.format(rank))
    df = _get_data(pd.read_csv(data_path), 'Antigen')
    print('Proc {} lectura lista!'.format(rank))

    if rank == 0:
        print('Dividiendo trabajo...')
        count = df.groupby(['Antigen']).size().reset_index(name='Count')
        antigen_list = count[count['Count'] >= antigen_rate]['Antigen'].values
        job = split(antigen_list, size)
        bonfer_coef = comb(len(antigen_list), 2)
    else:
        job = None
        bonfer_coef = None

    job = comm.scatter(job, root=0)

    bonfer_coef = comm.bcast(bonfer_coef, root=0)
    sig = 0.05
    file = open(os.path.join(result_dir, 'proc_{}_data.csv'.format(rank)), 'w')
    file_header = ['A', 'B', 'U_Test', 'P_Value', 'Status', 'Bonferroni_Status']
    file_writer = csv.DictWriter(file, fieldnames=file_header)
    file_writer.writeheader()

    print('Proc {} inciando trabajo'.format(rank))
    print(bonfer_coef)
    for i, j in job:
        s1 = df[df['Antigen'] == i]['Median_Value']
        s2 = df[df['Antigen'] == j]['Median_Value']
        u, p = stats.mannwhitneyu(s1, s2, alternative='two-sided')
        file_writer.writerow({'A': i, 'B': j, 'U_Test': u, 'P_Value': p, 'Status': 0})

    file.close()


main()
