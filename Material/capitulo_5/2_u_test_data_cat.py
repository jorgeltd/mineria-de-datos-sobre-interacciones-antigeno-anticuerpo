import pandas as pd
import os
import numpy as np
import capitulo_5.utils as ut
from scipy.special import comb
import networkx as nx
import sys


def save_matrix(edges, nodes, savepath, w=False):
    g = nx.Graph()
    g.add_nodes_from(nodes)
    if w:
        g.add_weighted_edges_from(edges)
    else:
        g.add_edges_from(edges)

    m = nx.adjacency_matrix(g, nodes).todense()
    df = pd.DataFrame(m)
    if w:
        df.to_csv(savepath, header=False, index=False, float_format='%.12E')
    else:
        df.to_csv(savepath, header=False, index=False)


def main():
    data_path = sys.argv[1]
    result_dir = sys.argv[2]
    data = None
    for root, dirs, files in os.walk(data_path):
        data = [pd.read_csv(os.path.join(root, file)) for file in files]

    df = pd.concat(data)
    # df = pd.read_csv('../data_set/u_test.csv')
    antigens = pd.concat([df['A'], df['B']], axis=0).unique()
    bonfer_coef = comb(len(antigens), 2)

    df['sig_bonfer@0.05'] = np.where(df['P_Value'] < (0.05 / bonfer_coef), 1, 0)
    df['sig_bonfer@0.01'] = np.where(df['P_Value'] < (0.01 / bonfer_coef), 1, 0)
    df['sig_bonfer@0.005'] = np.where(df['P_Value'] < (0.005 / bonfer_coef), 1, 0)
    df['sig@0.05'] = np.where(df['P_Value'] < 0.05, 1, 0)
    df['sig@0.01'] = np.where(df['P_Value'] < 0.01, 1, 0)
    df['sig@0.005'] = np.where(df['P_Value'] < 0.005, 1, 0)

    df = df.drop(['Status', 'Bonferroni_Status'], axis=1)
    df.to_csv(os.path.join(result_dir, 'u_test.csv'), float_format='%.12E', index=False)

    # Datos para grafos
    savedir = os.path.join(result_dir, 'grafos')
    ut.check_dir(savedir)

    edges = df[df['sig_bonfer@0.05'] == 0][['A', 'B']]
    edges_w = df[df['sig_bonfer@0.05'] == 0][['A', 'B', 'P_Value']]
    edges.to_csv(os.path.join(savedir, 'grafo_binario.txt'), index=False, header=False, sep='\t')
    edges_w.to_csv(os.path.join(savedir, 'grafo_con_pesos.txt'), index=False, header=False, float_format='%.12E', sep='\t')

    save_matrix(edges_w.values, antigens, os.path.join(savedir, 'adj_matrix.csv'), w=True)
    save_matrix(edges.values, antigens, os.path.join(savedir, 'adj_matrix_bin.csv'), w=False)

    nodes = pd.DataFrame(antigens)
    nodes.to_csv(os.path.join(savedir, 'nodes.txt'), sep='\t', header=False)

    print('Concat Done!')


main()
