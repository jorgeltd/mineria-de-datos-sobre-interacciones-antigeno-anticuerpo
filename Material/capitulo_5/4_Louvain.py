import pandas as pd
import os
import sys
import numpy as np
import networkx as nx
import community as louvain
from networkx.algorithms import community
import sys
from scipy.special import comb

data_path = sys.argv[1]
resolution = int(sys.argv[2])
df = pd.read_csv(data_path)

# Nodos son cada antigeno dentro de la data
antigens = pd.concat([df['A'], df['B']], axis=0).unique()

sig = 'sig_bonfer@0.01'

# 0 significa p >= sig, no se rechaza h0, las distribuciones son iguales
edges_w = df[df[sig] == 0][['A', 'B', 'P_Value']].values
edges = df[df[sig] == 0][['A', 'B']].values

# Louvain pesado

g = nx.Graph()
g.add_nodes_from(antigens)
g.add_weighted_edges_from(edges_w)

com = louvain.best_partition(g, resolution=resolution)
m = louvain.modularity(com, g)

print('Modularidad: {}'.format(m))
louvain_keys = set(com.values())
louvain_partition = [[node for node in com.keys() if com[node] == key] for key in louvain_keys]
print('-Louvain partitions: {}, sizes: '.format(louvain_keys), [len(x) for x in louvain_partition])
com = [[ant, group] for ant, group in com.items()]
df = pd.DataFrame(com, columns=['Antigen', 'Group'])
df.to_csv('louvain_groups.csv', index=False)
