networkx==2.2
backcall==0.1.0
biopython==1.72
configobj==5.0.6
cycler==0.10.0
decorator==4.3.0
editdistance==0.5.2
future==0.16.0
ipykernel==5.1.0
ipython==7.0.1
ipython-genutils==0.2.0
jedi==0.13.1
jupyter-client==5.2.3
jupyter-core==4.4.0
kiwisolver==1.0.1
matplotlib==3.0.0
mpltools==0.2.0
numpy==1.15.2
pandas==0.23.4
parso==0.3.1
pexpect==4.6.0
pickleshare==0.7.5
prompt-toolkit==2.0.5
ptyprocess==0.6.0
Pygments==2.2.0
pyparsing==2.2.2
python-dateutil==2.7.3
python-louvain==0.13
pytz==2018.5
pyzmq==17.1.2
scikit-learn==0.20.0
scipy==1.1.0
seaborn==0.9.0
simplegeneric==0.8.1
six==1.11.0
tornado==5.1.1
traitlets==4.3.2
wcwidth==0.1.7
mpi4py==3.0
