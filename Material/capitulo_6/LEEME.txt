### MATERIAL PARA EL DESARROLLO DEL CAPÍTULO 6 DEL PROYECTO

Scripts desarrollados con Python 3.6. Librerias externas descritas en el archivo
"requirements.txt".

La manera estándar de ejecución de los scripts es:
$ python3 "script" [1] [2] [...] [n]
donde [i] son argumentos del script

## ORDEN DE EJECUCIÓN

# 1 -- Script "1_cluster_analisis.py"
Script que genera los subgrupos de análisis descritos en la sección 6.1.1 del
escrito. Adicionalmente genera histogramas de frecuencias de anticuerpos para 
cada subgrupo y scatter plots de las señales presentes en cada cluster.

Entradas:
[1]:Archivo "dataset_clustering.csv" generado en el capítulo 5.
[2]:Directorio de salida.


2 -- Alineamiento Mega

Los alineamientos se realizaron utilizando el software MEGA, en su versión de 
escritorio, instalados en una distribución Linux. Se trabajó con el archivo de 
secuencias en formato fasta generados por el script "1_cluster_analisis.py" y 
se utilizó la herramienta Muscle para generar el alineamiento múltiple.

Los patrones se analizaron con las herramientas de visualización de conservación 
que presenta Mega, evaluando diferentes porcentajes de conservación.

3 -- Weblogos

Los web logos se generaron utilizando herramientas computacionales web: 
http://weblogo.threeplusone.com/create.cgi con los parámetros por default. 


4 -- MOSTT

Para el uso de MOSST se utilizó la versión web: http://pesb2.cl/MOSST/ y se 
dejó como entrada con el archivo de alineamiento de secuencias que entregó Mega. 
Se ejecutó sólo la primera versión debido a que se deseaba evaluar la divergencia 
de las secuencias y la ganancia de información en los stair plot.

5 -- Regiones epítopes

Todas las herramientas utilizadas son servicios web que utilizan como entradas 
archivos de secuencias en formato fasta. A continuación se dejan los links de estas.


Bepitope:       http://bepitope.ibs.fr/
Bepipred 1.0:   http://www.cbs.dtu.dk/services/BepiPred-1.0/
Bepipred 2.0:   http://www.cbs.dtu.dk/services/BepiPred/
CBTope:         http://crdd.osdd.net/raghava/cbtope/
ABCPred:        http://crdd.osdd.net/raghava/abcpred/

Los resultados se dejan adjuntos en el directorio "epitopos". 

6 -- Modelamiento anticuerpos y antígeno

Para la realizacion de modelos de anticuerpos y antigenos se utilizo RaptorX.

a) Para el anticuerpo, se buscó por medio de blastx alguna secuencia o estructura reportada.
b) Se seleccionó aquella secuencia con mayor porcentaje de identidad.
c) Se descargo esta secuencia y todos los aminoacidos x fueron reemplazados por Ala
*(esto debido a que RaptorX no acepta secuencias con caracteres diferentes a los 20 aa)
d) Se introdujo esta secuencia en Raptor X y se descargaron los resultados. 
e) Se comparo los templados reportados por Raptor X con la estructura resultante 
para observar si existe algun sentido biologico en la eleccion del templado y la
similitud de las estructuras (Estos resultados se reportan en su plataforma web).


# 7 -- Dockings

Los dockings fueron realizados con la herramienta web InterEvDock2
link http://bioserv.rpbs.univ-paris-diderot.fr/services/InterEvDock2/
Por cada par interacción simulada se selecionaron los mejores 2 resultados del
top 10 consesus y se descargaron los archivos pdb de estos. Con la herramienta 
Vmd se visualizaron las interacciones y se analizaron de forma manual distancias 
entre aminoacidos de que entran en interacción


