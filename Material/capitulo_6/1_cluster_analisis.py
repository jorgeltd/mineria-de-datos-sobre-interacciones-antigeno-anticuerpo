import pandas as pd
import numpy as np
import os
from Material.capitulo_6 import utils as ut
import sys
import matplotlib.pyplot as plt
import math
import seaborn as sns


# 1 = clusters data
# 2 = output dir
# 3 = cluster

# p = ../data_set/clustering_log/cluster_data/all.csv ../cluster_analisis/reso_1.0 37
# p = ../data_set/resolution_0.5/cluster_data/all.csv ../cluster_analisis/reso_0.5 46


# TODO MEJORAR TITULOS
def _regression_plot(_s, _labels, _title, _savedir):
    f = plt.figure(figsize=(16, 5))
    fit = np.polyfit(_s.index, _s, 1)
    fit_fn = np.poly1d(fit)
    plt.plot(_s.index, _s, 'yo', _s.index, fit_fn(_s.index), '--k')
    plt.plot(_s.index,_s, 'b*')
    plt.xticks(_s.index, _labels, rotation='vertical', fontsize=6)
    plt.subplots_adjust(bottom=0.2)
    plt.suptitle(_title)
    plt.savefig(_savedir, format='pdf')
    # f.show()
    plt.close(fig=f)


def _bar_chart(h, b, _title, savepath):
    f = plt.figure(figsize=(10, 5))
    y_pos = np.arange(len(b))

    # Create bars
    plt.bar(y_pos, h)

    # Rotation of the bars names
    plt.xticks(y_pos, b, rotation=90)

    # y ticks
    if max(h) < 15:
        yint = range(0, math.ceil(max(h)) + 1, 1)
    else:
        yint = range(0, math.ceil(max(h)) + 3, 2)

    plt.yticks(yint)

    # Custom the subplot layout
    plt.subplots_adjust(bottom=0.25, top=0.85)

    plt.suptitle(_title)
    plt.savefig(savepath, format='pdf')
    plt.close(fig=plt.gcf())
    # Show graphic
    #plt.show()


df_data = pd.read_csv(sys.argv[1])
df_data['Cluster_Z-Score'] = df_data.index


# Chart de frecuencia de anticuerpos dentro de un cluster

gp_cluster = df_data.groupby('Cluster')

for cluster, df in gp_cluster:
    print('...Cluster {}'.format(cluster))
    save_dir = os.path.join(sys.argv[2], 'cluster_{}/antibody_chart'.format(int(cluster)))
    ut.check_dir(save_dir)

    # interacciones fuertes
    cut_point = [1.5, 2, 3]

    for point in cut_point:
        data = df[df['Z-Score'] > point]
        ab_count = data.groupby('Antibody').size().sort_values()
        title = 'Frecuencias Anticuerpos Grupo CL_{}\nZ-Score > {}'.format(int(cluster), point)
        savepath = os.path.join(save_dir, 'cluster_{}_over_{}.pdf'.format(cluster, point))
        if ab_count.shape[0] > 0:
            _bar_chart(ab_count, ab_count.index, title, savepath)

    # Interacciones debiles
    cut_point = [0, -0.5, -1]

    for point in cut_point:
        data = df[df['Z-Score'] < point]
        ab_count = data.groupby('Antibody').size().sort_values()
        title = 'Frecuencias Anticuerpos Cluster {}\nZ-Score < {}'.format(int(cluster), point)
        savepath = os.path.join(save_dir, 'cluster_{}_under_{}.pdf'.format(cluster, point))
        if ab_count.shape[0] > 0:
            _bar_chart(ab_count, ab_count.index, title, savepath)

    # Extraer secuencias
    save_dir = os.path.join(sys.argv[2], 'cluster_{}/seq_z-score'.format(int(cluster)))
    ut.check_dir(save_dir)

    # Interacciones Fuertes
    cut_point = [3, 2, 1.5]

    for point in cut_point:
        seq = df[df['Z-Score'] > point][['Antigen', 'Antigen_Seq']]
        seq = seq.drop_duplicates('Antigen').reset_index(drop=True)
        savepath = os.path.join(save_dir, 'seq_cluster_{}_over_{}.fasta'.format(int(cluster), point))
        if seq.shape[0] > 0:
            f = open(savepath, 'w')
            for i, row in seq.iterrows():
                line = '>{}\n{}\n'.format(row[0], row[1])
                f.write(line)
            f.close()

    # Interacciones Debiles
    cut_point = [0, -0.5, -1]

    for point in cut_point:
        seq = df[df['Z-Score'] < point][['Antigen', 'Antigen_Seq']]
        seq = seq.drop_duplicates('Antigen').reset_index(drop=True)
        savepath = os.path.join(save_dir, 'seq_cluster_{}_under_{}.fasta'.format(int(cluster), point))
        if seq.shape[0] > 0:
            f = open(savepath, 'w')
            for i, row in seq.iterrows():
                line = '>{}\n{}\n'.format(row[0], row[1])
                f.write(line)
            f.close()


# Graficos intensidades

for cluster, df in gp_cluster:
    print('...Cluster {}'.format(cluster))
    save_dir = os.path.join(sys.argv[2], 'cluster_{}/graficos'.format(int(cluster)))
    ut.check_dir(save_dir)

    gp_antigen = df.groupby('Antigen')
    data = []
    data_header = ['Antigen', 'Mean', 'Median', 'Max', 'Min', 'Std']
    key = 'Median_Value'

    for antigen, sub_df in gp_antigen:
        antigen = antigen.split('~')[1]
        antigen = antigen.split(':')[-1]
        antigen = antigen.split('.')[0]
        mean = sub_df[key].mean()
        median = sub_df[key].median()
        maxi = sub_df[key].max()
        mini = sub_df[key].min()
        std = sub_df[key].std(ddof=0)
        data.append([antigen, mean, median, maxi, mini, std])

    for m in data_header[1:]:

        df_stat = pd.DataFrame(data, columns=data_header)
        df_stat = df_stat.sort_values(m).reset_index(drop=True)
        if df_stat.shape[0] > 1:
            save = os.path.join(save_dir, 'cluster_{}_medida_{}_{}.pdf'.format(int(cluster), m, key))
            _regression_plot(df_stat[m], df_stat['Antigen'], '{}() - Valor Logaritmo'.format(m), save)


