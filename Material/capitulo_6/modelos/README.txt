Para la realizacion de modelos de anticuerpos y antigenos se utilizo RaptorX.

a) Para el anticuerpo, se busc� por medio de blastx alguna secuencia o estructura reportada.
b) Se seleccion� aquella secuencia con mayor porcentaje de identidad.
c) Se descargo esta secuencia y todos los aminoacidos x fueron reemplazados por Ala
*(esto debido a que RaptorX no acepta secuencias con caracteres diferentes a los 20 aa)
d) Se introdujo esta secuencia en Raptor X y se descargaron los resultados. 
e) Se comparo los templados reportados por Raptor X con la estructura resultante 
para observar si existe algun sentido biologico en la eleccion del templado y la
similitud de las estructuras (Estos resultados se reportan en su plataforma web).


