import pandas as pd
import numpy as np
import os
import capitulo_4.utils as ut
import sys

# TODO HACER SUMARIO CANTIDAD DATOS FINAL

# INPUT 1 = directorio con data pasada por el filtro_3
# PARAMETERS
# ../data_cleaned/filtro_3_median_log2_median


def main():
    data_dir = sys.argv[1]
    controls_path = sys.argv[2]
    results_dir = sys.argv[3]
    ut.check_dir(results_dir)
    data = []

    for root, dirs, files in os.walk(data_dir):
        data_files = [x for x in files if 'proteins' in x]
        for file in data_files:
            print(file)
            df = pd.read_csv(os.path.join(root, file), sep='\t')
            df = df.drop_duplicates('Protein')

            data.append(df)

    df_all = pd.concat(data, ignore_index=True)
    bad_block = df_all['Bad_Block'] == True
    median_error_outlier = df_all['Median_Error_Outlier'] == True
    under_min = df_all['Log2_Median_Under_Min'] == True
    df_all['Bad_Data'] = np.where((bad_block | median_error_outlier | under_min), True, False)
    df_all = df_all.rename(index=str, columns={'Protein': 'Antigen', 'Protein_Sequence': 'Antigen_Seq', 'Log2_Median': 'Median_Value',
                                               'Log2_Mean': 'Mean_Value'})
    df_all = df_all[['Antibody', 'Antigen', 'Block', 'ID', 'Median_Value', 'Bad_Data', 'Antigen_Seq',
                     'Antibody_Ncl_Seq']]

    # UNOS DETALLES
    df_all.loc[df_all['Antigen_Seq'].str.contains('ISOFORM'), 'Antigen_Seq'] = 'MSEESDMDKAIKETSILEEYSINWTQKLGAGISGPVRVCVKKSTQERFALKILLDRP' \
                                                                               'KARNEVRLHMMCATHPNIVQIIEVFANSVQFPHESSPRARLLIVMEMMEGGELFHR' \
                                                                               'ISQHRHFTEKQASQVTKQIALALRHCHLLNIAHRDLKPENLLFKDNSLDAPVKLCD' \
                                                                               'FGFAKIDQGDLMTPQFTPYYVAPQVLEAQRRHQKEKSGIIPTSPTPYTYNKSCDLW' \
                                                                               'SLGVIIYVMLCGYPPFYSKHHSRTIPKDMRRKIMTGSFEFPEEEWSQISEMAKDVV' \
                                                                               'RKLLKVKPEERLTIEGVLDHPWLNSTEALDNVLPSAQLMMDKAVVAGIQQAHAEQL' \
                                                                               'ANMRIQDLKVSLKPLHSVNNPILRKRKLLGTKPKDSVYIHDHENGAEDSNVALEKL' \
                                                                               'RDVIAQCILPQAGENEDEKLNEVMQEAWKYNRECKLLRDTLQSFSWNGRGFTDKVD' \
                                                                               'RLKLAEIVKQVIEEQTTSHESQ'
    df_all['Antigen_Seq'] = df_all['Antigen_Seq'].str.replace('*', '')

    df_prot = df_all

    print('...Data Done!')
    df = pd.read_csv(controls_path, sep='\t')

    for col in df.columns:
        if '_' in col and 'Flag_Count' != col:
            df = df.drop(col, axis=1)
    df = df.drop(['Buffer-Empty', 'IG1-Buffer', 'IG1-Empty', 'IG3-Buffer', 'IG3-Empty', 'IG1-IG3'], axis=1)

    for col in df.columns:
        if col != 'Min' and col != 'File' and col != 'Block' and col != 'Flag_Count':
            print(col)
            df[col] = df[col] - df['Min']
    df = df[['File', 'Block', 'Flag_Count', 'Anti-human-IgG1', 'Anti-human-IgG2', 'Anti-human-IgG3', 'HumanIgG1', 'HumanIgG3']]
    df = df.rename(index=str, columns={'File': 'Antibody', 'Flag_Count': 'Bad_Data'})
    df['Antibody'] = df['Antibody'].str.replace('.tsv', '')
    ab_list = df_prot['Antibody'].unique()

    def _replace_ab(x):
        m = [s for s in ab_list if x in s]
        if len(m) > 1:
            print('ERROR CON ', m)
        return m[0]

    df['Antibody'] = df['Antibody'].apply(_replace_ab)

    # ARCHIVO CON TADOS SOLO CONTROLES INNECESARIO POR AHORA
    # df.to_csv(os.path.join(results_dir, 'controls.csv'), index=False, float_format='%.6f')
    print('...Controls Done!')

    # Archivo con datos de controles y antigenos para hacer el z-score
    df_control = df.melt(['Antibody', 'Block', 'Bad_Data'], var_name='Antigen', value_name='Median_Value')
    df_control['Bad_Data'] = df_control['Bad_Data'] > 0
    df_control['Type'] = 'Control'
    df_prot['Type'] = 'Antigen'
    df_all = pd.concat([df_prot, df_control], sort=True)
    df_all = df_all.sort_values(['Antibody', 'Block', 'Antigen']).reset_index(drop=True)
    df_all['Antigen'] = df_all['Antigen'].str.replace('N/A', 'NA')

    '''
    # Archivo con solo interaciones antigeno-anticuerpo
    df_prot = df_all[df_all['Type'] == 'Antigen']
    df_prot = df_prot.drop('Type', axis=1)
    df_prot.to_csv(os.path.join(results_dir, 'data_cleaned.csv'), index=False, float_format='%.6f')
    '''

    #####################################################################################
    # Calcular z-score por grupo
    #####################################################################################

    grp = df_all.groupby(['Antibody', 'Block'])

    for group,  sdf in grp:
        sdf = sdf[sdf['Bad_Data'] == False]
        mean = sdf['Median_Value'].mean()
        # sample std
        std = sdf['Median_Value'].std(ddof=1)
        z_score = sdf['Median_Value'].apply(lambda x: (x - mean)/std)
        df_all.loc[sdf.index, 'Z-Score'] = z_score

    df_all['Z-Class'] = np.where(df_all['Z-Score'] > 2, 2, np.where(df_all['Z-Score'] < -2, -2, 0))

    df_all = df_all[['Antibody', 'Antigen', 'Block', 'ID', 'Median_Value', 'Z-Score', 'Type', 'Bad_Data',
                     'Antigen_Seq', 'Antibody_Ncl_Seq']]
    df_all.to_csv(os.path.join(results_dir, 'dataset.csv'), index=False, float_format='%.6f')
    print('...Z Done!')


main()
