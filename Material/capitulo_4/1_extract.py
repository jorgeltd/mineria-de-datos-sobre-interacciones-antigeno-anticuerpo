# Generar tablas con valores normalizados para cada microarray
import numpy as np
import pandas as pd
import os
import capitulo_4.utils as ut
import sys
from Bio import SeqIO
from Bio.Alphabet import generic_dna


def _get_cv(s_1, s_2, dof=1):
    mean = (s_1 + s_2) / 2
    error = (s_1 - mean).apply(np.square) + (s_2 - mean).apply(np.square)
    # Aplicar sample standard deviation
    std = error / (2 - dof)
    std = std.apply(np.sqrt)
    cv = (std / mean) * 100
    return cv


def _get_antibodies(path):
    antibodies_list = []
    for seq_record in SeqIO.parse(path, "fasta", alphabet=generic_dna):
        '''
        if (len(seq_record) % 3) == 1:
             seq_record.seq = seq_record.seq[:-2]
        if (len(seq_record) % 3) == 2:
            seq_record.seq = seq_record.seq + 'N'
        # Puede no ser necesario traducir la secuencia de nucleotidos
        row = {'Ncl_Seq': str(seq_record.seq), 'Amino_Seq': str(seq_record.seq.translate()), 'Name': seq_record.name}
        '''
        row = {'Ncl_Seq': str(seq_record.seq), 'Name': seq_record.name}
        antibodies_list.append(row)
    df_antibodies = pd.DataFrame(antibodies_list)
    return df_antibodies


antibodies_path = sys.argv[1]
info_dir = sys.argv[2]
data_dir = sys.argv[3]
save_dir = sys.argv[4]

lotes = ['HA20073', 'HA20087']
bad_controls = ['Internal', 'AntiBiotinAb', 'BSA3', 'GST', 'BiotinAb9', 'Calmodulin', 'V5Control', 'Kinase1', 'MAPKAP', 'CMK2A']


# crear carpetas
ut.check_dir(save_dir)
ut.check_dir(os.path.join(save_dir, lotes[0]))
ut.check_dir(os.path.join(save_dir, lotes[1]))

antibodies = _get_antibodies(antibodies_path)

for root, dirs, files in os.walk(data_dir):
    lot = root.split('/')[-1]
    if lot in lotes:
        df_seq = pd.read_csv(info_dir + '/' + lot + '_seq.tsv', sep='\t', index_col='Accno')
        df_seq['Sequence'] = df_seq['Sequence'].str.upper()
        df_seq = df_seq[df_seq.index.duplicated() == False]

    for file in files:
        print(file)
        df = pd.read_csv(os.path.join(root, file), sep='\t')
        df['F-B_Median'] = df.index
        df['F-B_Mean'] = df.index
        df['F-B_Log2_Mean'] = df.index
        df['F-B_Log2_Median'] = df.index
        df['Log2_Mean'] = df.index
        df['Log2_Median'] = df.index
        df['Minmax_Mean'] = df.index
        df['Minmax_Median'] = df.index
        df['F-B_Median'] = df.index
        df['Protein_Sequence'] = df.index
        df['Protein_Len'] = 0
        df['RFU'] = '-'

        # Relative fluorescence units
        df['RFU'] = df['Name'].apply(lambda x: float(x.split('~')[-1].split(':')[-1])
                                     if (len(x.split('~')) > 1 and x.split('~')[-1] != 'N/A') else np.NaN)

        # Remover RFU del Nombre
        df['Name'] = df['Name'].apply(lambda x: '~'.join(x.split('~')[:-1]) if len(x.split('~')) > 1 else x)
        df = df.rename(columns={'Name': 'Protein'})

        # Borrar controles no utiles
        for control in bad_controls:
            df = df[df['Protein'].str.contains(control) == False]

        # Restar foreground - background
        df[['F-B_Mean']] = df['F635_Mean'] - df['B635_Mean']
        df[['F-B_Median']] = df['F635_Median'] - df['B635_Median']

        # F-B Normalizar usando log2
        df['F-B_Log2_Median'] = df['F-B_Median'].clip(1, 65536).apply(np.log2)
        df['F-B_Log2_Mean'] = df['F-B_Mean'].clip(1, 65536).apply(np.log2)
        df['Log2_Median'] = df['F635_Median'].apply(np.log2)
        df['Log2_Mean'] = df['F635_Mean'].apply(np.log2)

        # Minmax Intesidades
        df['Minmax_Median'] = df['F635_Median']
        df['Minmax_Median'] -= df['Minmax_Median'].min()
        df['Minmax_Median'] /= df['Minmax_Median'].max()

        df['Minmax_Mean'] = df['F635_Mean']
        df['Minmax_Mean'] -= df['Minmax_Mean'].min()
        df['Minmax_Mean'] /= df['Minmax_Mean'].max()

        # Botar columnas
        df = df.drop(['X', 'Y', 'Description', 'Dia', 'F_Pixels', 'B_Pixels'], 1)

        # Añadir secuencias proteinas humanas
        df['Protein_Sequence'] = df['Protein'].map(df_seq['Sequence'], na_action='ignore')
        df['Protein_Len'] = df['Protein_Sequence'].str.len() - 1

        # Añadir secuencias anticuerpos
        antibody = antibodies[antibodies['Name'].str.contains(file.split('.')[0]) == True]
        if antibody.shape[0] == 0:
            print('error secuencia archivo ', file)

        df['Antibody'] = antibody['Name'].values[0]
        df['Antibody_Ncl_Seq'] = antibody['Ncl_Seq'].values[0]

        # Elegir proteinas humanas
        df_proteins = df[df['Protein'].str.contains('Hs~')]
        # Eliminar proteinas sin sequencia
        df_proteins = df_proteins[df_proteins['Protein_Len'] > 0]

        # Df para medidas de errores
        df_sample_1 = df_proteins.iloc[::2].reset_index(drop=True)
        df_sample_2 = df_proteins.iloc[1::2].reset_index(drop=True)
        df_errors = df_sample_1[['Block', 'Row', 'Column', 'Protein', 'ID']].reset_index(drop=True)

        df_errors['F635_Median_CV'] = _get_cv(df_sample_1['F635_Median'], df_sample_2['F635_Median'])
        df_errors['F635_Mean_CV'] = _get_cv(df_sample_1['F635_Mean'], df_sample_2['F635_Mean'])

        df_errors['F635_Median_L1'] = (df_sample_1['F635_Median'] - df_sample_2['F635_Median']).apply(np.abs)
        df_errors['F635_Mean_L1'] = (df_sample_1['F635_Mean'] - df_sample_2['F635_Mean']).apply(np.abs)

        df_errors['Log2_Median_L1'] = (df_sample_1['Log2_Median'] - df_sample_2['Log2_Median']).apply(np.abs)
        df_errors['Log2_Mean_L1'] = (df_sample_1['Log2_Mean'] - df_sample_2['Log2_Mean']).apply(np.abs)

        # Error sin abs para generar una distribucion normal
        df_errors['Log2_Median_L1_noAbs'] = df_sample_1['Log2_Median'] - df_sample_2['Log2_Median']
        df_errors['Log2_Mean_L1_noAbs'] = df_sample_1['Log2_Mean'] - df_sample_2['Log2_Mean']

        df_errors['Minmax_Median_L1'] = (df_sample_1['Minmax_Median'] - df_sample_2['Minmax_Median']).apply(np.abs)
        df_errors['Minmax_Mean_L1'] = (df_sample_1['Minmax_Mean'] - df_sample_2['Minmax_Mean']).apply(np.abs)

        # Elegir controles
        df_controls = df[df['Protein'].str.contains('Hs~') == False]

        # Guardar dataframes
        all_savepath = save_dir + root.split('/')[-1] + '/' + 'all_' + file.split('.')[0] + '.tsv'
        ctrl_savepath = save_dir + root.split('/')[-1] + '/' + 'ctrls_' + file.split('.')[0] + '.tsv'
        protein_savepath = save_dir + root.split('/')[-1] + '/' + 'proteins_' + file.split('.')[0] + '.tsv'
        error_savepath = save_dir + root.split('/')[-1] + '/' + 'errors_' + file.split('.')[0] + '.tsv'

        df.to_csv(all_savepath, sep='\t', index=False, float_format='%.6f')
        df_controls.to_csv(ctrl_savepath, sep='\t', index=False, float_format='%.6f')
        df_proteins.to_csv(protein_savepath, sep='\t', index=False, float_format='%.6f')
        df_errors.to_csv(error_savepath, sep='\t', index=False, float_format='%.6f')
