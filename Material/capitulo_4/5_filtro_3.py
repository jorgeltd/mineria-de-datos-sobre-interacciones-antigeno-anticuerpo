import pandas as pd
import numpy as np
import os
import capitulo_4.utils as ut
import sys
from shutil import copy2


# PARAMETERS = ../data_cleaned/filtro_2_median/ .
#
#
# sv filtro_3_median_log2_median/
def main():
    data_dir = sys.argv[1]
    path_flags = sys.argv[2]
    results_dir_base = sys.argv[3]
    results_dir = sys.argv[4]
    ut.check_dir(results_dir)
    ut.check_dir(results_dir_base)
    key = 'Log2_Median'
    error_type = 'Median_Error_Outlier'
    summary_data = []
    summary_header = ['File', 'Block', 'Discarted_Count']

    df_flags = pd.read_csv(path_flags, sep='\t')

    for root, dirs, files in os.walk(data_dir):
        protein_files = [x for x in files if 'protein' in x]
        other_files = [x for x in files if x not in protein_files and 'all' not in x]
        for file in other_files:
            copy2(os.path.join(root, file), results_dir)

        for file in protein_files:
            microarray = file.split('_')[-1]
            print(microarray)

            df = pd.read_csv(os.path.join(root, file), sep='\t')

            # Reducir los datos de duplicados a datos unicos mediante promedios
            sample_1 = df.iloc[::2].reset_index(drop=True)
            sample_2 = df.iloc[1::2].reset_index(drop=True)

            tmp = sample_1.loc[:, ['Block', 'Protein', 'ID', 'Protein_Sequence', 'Protein_Len', 'Antibody',
                                   'Antibody_Ncl_Seq', 'Bad_Block', error_type]]
            tmp['Log2_Median'] = (sample_1['Log2_Median'] + sample_2['Log2_Median']) / 2
            tmp['Log2_Mean'] = (sample_1['Log2_Mean'] + sample_2['Log2_Mean']) / 2
            df = tmp

            groupby = df.groupby('Block')

            for block, group in groupby:
                group = group[(group['Bad_Block'] == False) & (group['Median_Error_Outlier'] == False)]
                minimum = df_flags.loc[(df_flags['File'] == microarray) & (df_flags['Block'] == block), 'Min'].tolist()
                df.loc[group.index, key + '_Under_Min'] = group[key] < minimum
                data_row = [microarray, block, np.sum((group[key] < minimum))]
                df.loc[group.index, key] = group[key] - minimum
                summary_data.append(data_row)
            df.to_csv(os.path.join(results_dir, file), sep='\t', index=False, float_format='%.6f')

    df_summary = pd.DataFrame(summary_data, columns=summary_header)
    df_summary.to_csv(os.path.join(results_dir_base, 'filtro_3_summary_discarted_by_{}.tsv'.format(key)), index=False, sep='\t')


main()
