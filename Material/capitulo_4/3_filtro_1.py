import numpy as np
import pandas as pd
import os
import sys
import capitulo_4.utils as ut


data_dir = sys.argv[1]
path_filter_table = sys.argv[2]
results_dir = sys.argv[3]

ut.check_dir(results_dir)
df_filter = pd.read_csv(path_filter_table, sep='\t')


for root, dirs, files in os.walk(data_dir):
    lote = root.split('/')[-1]
    for file in files:
        microarray = file.split('_')[-1]
        print(file, lote)
        df = pd.read_csv(os.path.join(root, file), sep='\t')

        df_microarray = df_filter[df_filter['File'] == microarray]
        is_bad = np.where(df_microarray['Flag_Count'] > 0, True, False)
        dict_map = dict(zip(df_microarray['Block'], is_bad))
        df['Bad_Block'] = df['Block'].map(dict_map)

        df.to_csv(os.path.join(results_dir, file), sep='\t', index=False, float_format='%.6f')
